import React from 'react';
import styled from 'styled-components';
import BottomBar from '../components/BottomBar';
import LeftBar from '../components/LeftBar';
import RightBar from '../components/RightBar'
import TopBar from '../components/TopBar';
import LoginClient from '../components/LoginClient';

const Container = styled.div({
  display: "flex",
  width: "100%",
  height: "100%",
  top:0
});

const Home = () => {
  return (
    <>
      <TopBar />
      <Container>
        <LeftBar />
        <LoginClient />
        <RightBar />
      </Container>
      <BottomBar />
    </>
  );

}


export default Home;
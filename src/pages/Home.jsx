import React from 'react';
import styled from 'styled-components';
import BottomBar from '../components/BottomBar';
import LeftBar from '../components/LeftBar';
import RightBar from '../components/RightBar'
import TopBar from '../components/TopBar';
import HomeClient from '../components/HomeClient';

const Container = styled.div({
display: "flex",
width: "100%",
top:0,
});

const Home = () => {
  return (
    <>
      <TopBar />
      <Container>
        <LeftBar />
        <HomeClient />
        <RightBar />
      </Container>
      <BottomBar />
    </>
  );

}


export default Home;
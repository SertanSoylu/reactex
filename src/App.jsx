import { Route, BrowserRouter, Routes } from 'react-router-dom';
import Login from './pages/Login'
import Home from './pages/Home';
import Settings from './pages/Settings';

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/settings" element={<Settings />} />
      </Routes>
    </BrowserRouter>

  );
}

export default App;

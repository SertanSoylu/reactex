import axios from "axios";

export const fetchPosts = (userId) => {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.get(
        `${process.env.REACT_APP_API_URL}/posts?userId=${userId}`
      );
      resolve(response.data);
    } catch (error) {
      reject(error);
    }
  });
};

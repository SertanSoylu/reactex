import axios from "axios";

export const fetchUsers = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios.get(
        process.env.REACT_APP_API_URL + "/users"
      );
      resolve(response.data);
    } catch (error) {
      reject(error);
    }
  });
};

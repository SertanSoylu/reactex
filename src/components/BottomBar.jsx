import React from "react";
import styled from "styled-components";

const Container = styled.div`
  height: 50px;
  top: calc(100vh - 50px);
  width: 100%;
  position: fixed;
  align-items: center;
  display: flex;
  justify-content: center;
  background-color: gray;
  color: white;
  font-weight: bold[300];
  font-size: 25px;
`;

const BottomBar = () => {
  return <Container>SoylusofT</Container>;
};

export default BottomBar;

import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { fetchUsers } from "../api/userApi";
import { updateLastUser } from "../redux/AppSlice";

const Container = styled.div`
  flex: 2;
  height: calc(100 - 50px - 50px);
  background-color: #c2f5c2;
  align-items: center;
`;
const Wrapper = styled.div`
  margin-top: 20px;
`;

const List = styled.ul({
  listStyle: "none",
  paddingLeft: "10px",
});

const ListItem = styled.li({
  marginBottom: "10px",
  display: "flex",
  alignItems: "center",
  justifyContent: "left",
  cursor: "pointer",
});

const Avatar = styled.img({
  width: "35px",
  height: "35px",
  borderRadius: "50%",
  border: "2px solid white",
  objectFit: "cover",
});

const UserTitle = styled.span({
  marginLeft: "10px",
  color: "GrayText",
  fontSize: "18px",
  fontWeight: "bold",
});

const RightBar = () => {
  const [users, setUsers] = useState([]);

  const dispatch = useDispatch();

  const ListItemClick = (uid) => {
    const selectedUser = users.filter((v, i) => v.id === uid)[0];
    dispatch(updateLastUser(selectedUser));
  };

  useEffect(() => {
    fetchUsers()
      .then((res) => {
        setUsers(res);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  return (
    <Container>
      <Wrapper>
        <List>
          {users.map((u) => {
            return (
              <ListItem key={u.id} onClick={() => ListItemClick(u.id)}>
                <Avatar
                  src={
                    u.id % 2 === 1
                      ? "https://www.w3schools.com/howto/img_avatar.png"
                      : "https://www.w3schools.com/howto/img_avatar2.png"
                  }
                ></Avatar>
                <UserTitle>{u.name}</UserTitle>
              </ListItem>
            );
          })}
        </List>
      </Wrapper>
    </Container>
  );
};

export default RightBar;

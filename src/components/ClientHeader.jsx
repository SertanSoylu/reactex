import styled from 'styled-components';

const Header = styled.div`

`
const ClientHeader = (props) => {
    return (
    <Header>
        <h1>{props.title}</h1>
        <hr color="crimson" />
    </Header>

  );
}

export default ClientHeader
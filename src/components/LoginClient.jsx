import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { update, remove } from '../redux/UserSlice';
import ClientHeader from './ClientHeader';

const Container = styled.div`
    flex: 3;
    margin: 5px;
`
const Wrapper = styled.div`
`
const Client = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 10px;
`

const Input = styled.input`
    padding: 10px;
    margin : 5px;
`

const ButtonPanel = styled.div`
    display: flex;
    justify-content: space-evenly;
    margin: 5px;
`

const Button = styled.button`
    flex: 2;
    padding: 10px;
    margin: 5px;
`

const LoginClient = () => {

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");

    const stateName = useSelector((state) => state.userSlice.name);
    const stateEmail = useSelector((state) => state.userSlice.email);
    const dispatch = useDispatch();

    const handleClick = (e) => {
        e.preventDefault();
        dispatch(update({ name, email }));
    }

    const handleRemoveClick = (e) => {
        e.preventDefault();
        dispatch(remove());
    }

    return (
        <Container>
            <Wrapper>
                <ClientHeader title="Login" />
                <Client>
                    <Input
                        type="text"
                        //ref={name}
                        placeholder='Name Please...'
                        onChange={(e) => setName(e.target.value)}
                    />
                    <Input
                        type="text"
                        //ref={email}
                        placeholder='e-Mail Please...'
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <ButtonPanel>
                        <Button
                            onClick={handleClick}
                        >
                            Set Session State
                        </Button>
                        <Button
                            onClick={handleRemoveClick}
                        >
                            Remove Session State
                        </Button>
                    </ButtonPanel>
                    <br />
                    <br />
                    Welcome Dear,
                    <br />
                    {stateName}
                    <br />
                    {stateEmail}
                </Client>
            </Wrapper>
        </Container>
    );
}

export default LoginClient;
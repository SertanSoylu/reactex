import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { update, remove } from '../redux/UserSlice';
import ClientHeader from './ClientHeader';

const Container = styled.div`
    flex: 3;
    display: flex;
    flex-direction: column;
    margin: 5px;
    
`

const SettingsClient = () => {

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");

    const stateName = useSelector((state) => state.userSlice.name);
    const stateEmail = useSelector((state) => state.userSlice.email);

    return (
        <Container>
            <ClientHeader title="Settings" />
            <br />
            {stateName}
            <br />
            {stateEmail}
        </Container>
    );
}

export default SettingsClient;
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { fetchPosts } from '../api/postApi';
import ClientHeader from './ClientHeader';
import Post from './Post';

const Container = styled.div`
    flex: 3;
    display: flex;
    flex-direction: column;
    margin: 5px;
    height: calc(100vh - 50px - 50px);
    overflow-y: scroll;
    &::-webkit-scrollbar {
        display: none;
    }


`
const Wrapper = styled.div`
    margin-top: 10px;
    height: 100%;
`

const HomeClient = () => {
    const [posts, setPosts] = useState([]);
    const lastUser = useSelector(state => state.appSlice.lastUser);

    useEffect(() => {
        lastUser && fetchPosts(lastUser.id)
            .then(res => {
                setPosts(res);
                //console.log("lastUser", this.lastUser);
                //console.log("posts", posts);
            })
            .catch(err => {
                console.log(err);
            });
    }, [lastUser]);

    return (
        <Container>
            <ClientHeader title="Home" />
            <Wrapper>
                {posts.map((p) => {
                    return (
                        <Post key={p.id} post={p} />
                    );
                })}
                {posts.length === 0
                    ?
                    <Post post={{
                        id: 0,
                        title: 'Posts',
                        body: 'Listeden Kullanıcı Seçiniz'
                    }}
                    />
                    : <Post post={{}} />
                }
            </Wrapper>
        </Container>
    );
}

export default HomeClient;
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { Person, Settings } from '@mui/icons-material';
import { Link } from "react-router-dom";

const Container = styled.div({
    height: "50px",
    width: "100vw",
    backgroundColor: "darkcyan",
});

const Wrapper = styled.div({
    display: "flex",
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
})

const StyledLink = styled(Link)`
    text-decoration: none;
`

const Left = styled.div({
    flex: 1,
    paddingLeft: "20px",
});

const Logo = styled.span({
    fontSize: "20px",
    fontWeight: "bold",
    color: 'white',
    letterSpacing: 3,
});

const Center = styled.div({
    flex: 2,
    color: "white",
    fontWeight: "bold",
    fontSize: "40px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
});

const Right = styled.div({
    flex: 1,
    display: "flex",
    justifyContent: "flex-end",
    paddingRight: "20px",
});

const Icon = styled.div({
    color: "white",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: "15px",
    cursor: 'pointer',
});

const TopBar = () => {
    const lastUser = useSelector((state) => state.appSlice.lastUser);

    return (
        <Container>
            <Wrapper>
                <Left>
                    <StyledLink to="/"><Logo>ReactEX</Logo></StyledLink>
                </Left>
                <Center>{lastUser && `${lastUser.name}`}</Center>
                <Right>
                    <Link to="/login"><Icon><Person /></Icon></Link>
                    <Link to="/settings"><Icon><Settings /></Icon></Link>
                </Right>
            </Wrapper>
        </Container>
    );
}

export default TopBar;

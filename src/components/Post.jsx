import React from 'react'
import styled from 'styled-components';

const Container = styled.div({
    //    height: "250px",
    marginBottom: "20px",
});

const Wrapper = styled.div({
    padding: "10px",
    display: "flex",
    flexDirection: "column",
    border: "1px solid lightgray",
    borderRadius: "0.5em",
});

const From = styled.div({
    fontWeight: 'bold',
    marginBottom: "5px",
});

const Title = styled.div({
    marginTop: "5px",
    fontWeight: "bold",
});

const Body = styled.div({
    padding: "20px",
});


const Post = (props) => {
    return (
        <Container>
            <Wrapper>
                <From>
                    {props.post.userId}
                </From>
                <hr />
                <Title>
                    {props.post.title}
                </Title>
                <Body>
                    {props.post.body}
                </Body>
            </Wrapper>
        </Container>
    )
}

export default Post
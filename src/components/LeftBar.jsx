import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

const Container = styled.div`
  flex: 1;
  background-color: #ebebcb;
  height: calc(100vh - 50px - 50px);
  overflow-y: scroll;
  &::-webkit-scrollbar {
    background-color: gray;
    width: 4px;
  } 
  &::-webkit-scrollbar-track {
    background-color: white;
  }
  &::-webkit-scrollbar-thumb {
    background-color: gray;
  }
`

const Wrapper = styled.div`
  padding-top: 20px;
`

const Menu = styled.ul`
  list-style-type: none;
`

const MenuItem = styled.li`
  margin-bottom: 10px;
  cursor: pointer;
  font-size: 20px;
`

const LeftBar = () => {
  const stateName = useSelector((state) => state.userSlice.name);
  const stateEmail = useSelector((state) => state.userSlice.email);
  return (

    <Container>
      <Wrapper>
        <Menu>
          <MenuItem>
            Posts
          </MenuItem>
          <MenuItem>
            Albums
          </MenuItem>
          <MenuItem>
            Todos
          </MenuItem>
        </Menu>
      </Wrapper>
    </Container>
  );

}

export default LeftBar;

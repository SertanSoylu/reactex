import { configureStore } from '@reduxjs/toolkit';
import userSlice from './UserSlice';
import appSlice from './AppSlice';

export default configureStore({
    reducer: {
        appSlice,
        userSlice,
    }
}); 


import { createSlice } from "@reduxjs/toolkit";

const appSlice = createSlice({
  name: "app",
  initialState: {
    lastUser: null,
  },
  reducers: {
    updateLastUser: (state, action) => {
      state.lastUser = action.payload;
    },
    removeLastUser: (state) => {
      state.lastUser = null;
    },
  },
});

export const { updateLastUser, removeLastUser } = appSlice.actions;
export default appSlice.reducer;
